<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language ?>" xml:lang="<?php print $language ?>">

	<head>
	  <title><?php print $head_title ?></title>
	  <?php print $head ?>
	  <?php print $styles ?>
	  <?php print $scripts ?>
	  <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyle Content in IE */ ?> </script>
	</head>
	
	<body>
		<div id="outer">
			<div id="top-header"><?php print $header ?></div>
			<div id="header">
				<div id="logo">
					<!-- Place your logo here -->
					<?php if ($logo) { ?><a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><img src="<?php print $logo ?>" alt="<?php print t('Home') ?>" /></a><?php } ?>
				</div>

				<!-- Sign In Box -->
				<div id="login-box">
					<?php
						global $user;
						$this_output = "";
						if (!$user->uid) {                                                          
							$this_output .= drupal_get_form('user_login_block');                           
						}                                                                           
						else {                                                                      
							$this_output .= t('<p class="user-info">Hi !user, welcome back.</p>', array('!user' => theme('username', $user)));
						}
						print $this_output;	
					?>	
				</div>
			</div>
			<div id="top-nav" class="clearfix">
				<!-- Main Navigation -->
				<?php if (isset($primary_links)) { ?><?php print theme('links', $primary_links, array('class' =>'links', 'id' => 'navlist')) ?><?php } ?>
				<?php if (isset($secondary_links)) { ?><?php print theme('links', $secondary_links, array('class' =>'links', 'id' => 'subnavlist')) ?><?php } ?>
			</div>
			<div id="body-middle" class="clearfix">
			
				<!-- Main Column -->
				<div id="main-col">
				
					<!-- Nameplate Box -->
					<div id="nameplate-top"></div>
					<div id="nameplate-middle" class="clearfix">
						<?php if ($site_name) { ?><h1 class='site-name'><a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><?php print $site_name ?></a></h1><?php } ?>
						<?php if ($site_slogan) { ?><div class='site-slogan'><?php print $site_slogan ?></div><?php } ?>
					</div>
					<div id="nameplate-bottom"></div>

					<?php if ($mission) { ?><div id="mission"><?php print $mission ?></div><?php } ?>

					<!-- Main Content -->
					<div id="content-top"></div>
					<div id="content-wrapper">
						<div id="main">
							<?php print $breadcrumb ?>
							<h1 class="title"><?php print $title ?></h1>
							<div class="tabs"><?php print $tabs ?></div>
							<?php print $help ?>
							<?php print $messages ?>
							<?php print $content; ?>
							<?php print $feed_icons; ?>
						</div>
					</div>
					<div id="content-bottom"></div>
					
				</div>
				<!-- End Main Column -->

				<!-- Sidebar Content -->
				<div id="side-col">

					<?php if ($sidebar_left) { print $sidebar_left; } ?>
					<?php if ($sidebar_right) { print $sidebar_right; } ?>
					
				</div>
				<!-- End Sidebar Content -->
			</div>
			<div id="body-bottom"></div>

			<!-- Footer -->
			<div id="footer">
				<p><a href="http://www.arbutusphotography.com">victoria</a> | <a href="http://www.studio7designs.com">design by studio7designs</a> | <a href="http://www.slicendiceit.com">coding by slice 'n dice</a> | <a href="http://drupal.org/user/39343">Ported by Jason Swaby</a></p>
				<?php print $footer_message ?>
			</div>
		</div>

	<?php print $closure ?>
	</body>
</html>
